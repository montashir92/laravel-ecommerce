<?php

use Illuminate\Support\Facades\Route;

//Frontend
use App\Http\Controllers\Frontend\PagesController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\CartsController;
use App\Http\Controllers\Frontend\CheckoutsController;
use App\Http\Controllers\Frontend\DashboardController;

//Backend
use App\Http\Controllers\Backend\UsersController;
use App\Http\Controllers\Backend\ProfilesController;
use App\Http\Controllers\Backend\CategoriesController;
use App\Http\Controllers\Backend\BrandsController;
use App\Http\Controllers\Backend\ColorsController;
use App\Http\Controllers\Backend\SizesController;
use App\Http\Controllers\Backend\ProductsController;
use App\Http\Controllers\Backend\SlidersController;
use App\Http\Controllers\Backend\CustomerController;
use App\Http\Controllers\Backend\OrdersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', [PagesController::class, 'index'])->name('index');
Route::get('/contact', [PagesController::class, 'contact'])->name('contacts');


//Product Route for frontend
Route::prefix(md5('products'))->group(function(){
    Route::get('/', [ProductController::class, 'index'])->name('products');
    Route::get('/show/{slug}', [ProductController::class, 'show'])->name('product.show');
    Route::post('/product/search', [ProductController::class, 'search'])->name('product.search');
    Route::get('/product-get', [ProductController::class, 'getProduct'])->name('product.get');
    
    //Category Product
    Route::get('/categories/{category_id}', [ProductController::class, 'categoryProduct'])->name('categories.show');
    Route::get('/brands/{brand_id}', [ProductController::class, 'brandProduct'])->name('brands.show');
});

//Cart Routes
Route::prefix(md5('carts'))->group(function(){
    Route::get('/', [CartsController::class, 'index'])->name('carts');
    Route::post('/store', [CartsController::class, 'store'])->name('cart.store');
    Route::post('/update', [CartsController::class, 'update'])->name('cart.update');
    Route::get('/delete/{id}', [CartsController::class, 'delete'])->name('cart.delete');
});

//Customer Signup Process
Route::get('/customer-login', [CheckoutsController::class, 'index'])->name('customer.login');
Route::get('/customer-signup', [CheckoutsController::class, 'signup'])->name('customer.signup');
Route::post('/signup-store', [CheckoutsController::class, 'signupStore'])->name('customer.signup.store');
Route::get('/email-verify', [CheckoutsController::class, 'emailVerify'])->name('customer.email.verify');
Route::post('/verify-store', [CheckoutsController::class, 'verifyStore'])->name('customer.verify.store');
Route::get('/checkouts', [CheckoutsController::class, 'checkOut'])->name('customer.checkouts');
Route::post('/checkout/store', [CheckoutsController::class, 'checkStore'])->name('customer.checkout.store');



Auth::routes();

//Customer Dashboard Route
Route::group(['middleware' => ['auth', 'customer']], function(){
    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('customer.dashboard');
    Route::get('/edit-profile', [DashboardController::class, 'edit'])->name('customer.edit.profile');
    Route::post('/update-profile', [DashboardController::class, 'update'])->name('customer.update.profile');
    Route::get('/change-password', [DashboardController::class, 'changePassword'])->name('customer.change.password');
    Route::post('/update-password', [DashboardController::class, 'updatePassword'])->name('customer.update.password');
   
        Route::get('/payment', [DashboardController::class, 'payment'])->name('customer.payment');
        Route::post('/payment/store', [DashboardController::class, 'paymentStore'])->name('customer.payment.store');
        Route::get('/order-list', [DashboardController::class, 'orderList'])->name('customer.order.list');
        Route::get('/order-details/{id}', [DashboardController::class, 'orderDetails'])->name('customer.order.details');
        Route::get('/order-print/{id}', [DashboardController::class, 'orderPrint'])->name('customer.order.print');
    
});

//Admin Routes
Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    
    //User Routes
    Route::prefix('users')->group(function(){
        Route::get('/', [UsersController::class, 'index'])->name('user.index');
        Route::get('/create', [UsersController::class, 'create'])->name('user.create');
        Route::post('/store', [UsersController::class, 'store'])->name('user.store');
        Route::get('/edit/{id}', [UsersController::class, 'edit'])->name('user.edit');
        Route::post('/update/{id}', [UsersController::class, 'update'])->name('user.update');
        Route::post('/user/delete', [UsersController::class, 'delete'])->name('user.delete');
    });
    
    //User Profile Routes
    Route::prefix('profiles')->group(function(){
        Route::get('/', [ProfilesController::class, 'index'])->name('user.profiles');
        Route::get('/edit', [ProfilesController::class, 'edit'])->name('user.profile.edit');
        Route::post('/update', [ProfilesController::class, 'update'])->name('user.profile.update');
        Route::get('/user/change-password', [ProfilesController::class, 'changePassword'])->name('user.change.password');
        Route::post('/user/update-password', [ProfilesController::class, 'updatePassword'])->name('user.update.password');
    });
    
    //Customer Routes
    Route::prefix('customers')->group(function(){
        Route::get('/', [CustomerController::class, 'index'])->name('customer.show');
        Route::get('/draft/view', [CustomerController::class, 'draftView'])->name('customer.draft.show');
        Route::post('/customer/delete', [CustomerController::class, 'delete'])->name('customer.draft.delete');
    });
    
    //Categories Routes
    Route::prefix('categories')->group(function(){
        Route::get('/', [CategoriesController::class, 'index'])->name('categories.index');
        Route::get('/create', [CategoriesController::class, 'create'])->name('categories.create');
        Route::post('/store', [CategoriesController::class, 'store'])->name('categories.store');
        Route::get('/edit/{id}', [CategoriesController::class, 'edit'])->name('categories.edit');
        Route::post('/update/{id}', [CategoriesController::class, 'update'])->name('categories.update');
        Route::post('/category/delete', [CategoriesController::class, 'delete'])->name('categories.delete');
    });
    
    //Brand Routes
    Route::prefix('brands')->group(function(){
        Route::get('/', [BrandsController::class, 'index'])->name('brands.index');
        Route::get('/create', [BrandsController::class, 'create'])->name('brands.create');
        Route::post('/store', [BrandsController::class, 'store'])->name('brands.store');
        Route::get('/edit/{id}', [BrandsController::class, 'edit'])->name('brands.edit');
        Route::post('/update/{id}', [BrandsController::class, 'update'])->name('brands.update');
        Route::post('/brand/delete', [BrandsController::class, 'delete'])->name('brands.delete');
    });
    
    //Color Routes
    Route::prefix('colors')->group(function(){
        Route::get('/', [ColorsController::class, 'index'])->name('colors.index');
        Route::get('/create', [ColorsController::class, 'create'])->name('colors.create');
        Route::post('/store', [ColorsController::class, 'store'])->name('colors.store');
        Route::get('/edit/{id}', [ColorsController::class, 'edit'])->name('colors.edit');
        Route::post('/update/{id}', [ColorsController::class, 'update'])->name('colors.update');
        Route::post('/color/delete', [ColorsController::class, 'delete'])->name('colors.delete');
    });
    
    //Size Routes
    Route::prefix('sizes')->group(function(){
        Route::get('/', [SizesController::class, 'index'])->name('sizes.index');
        Route::get('/create', [SizesController::class, 'create'])->name('sizes.create');
        Route::post('/store', [SizesController::class, 'store'])->name('sizes.store');
        Route::get('/edit/{id}', [SizesController::class, 'edit'])->name('sizes.edit');
        Route::post('/update/{id}', [SizesController::class, 'update'])->name('sizes.update');
        Route::post('/size/delete', [SizesController::class, 'delete'])->name('sizes.delete');
    });
    
    //Product Routes
    Route::prefix('products')->group(function(){
        Route::get('/', [ProductsController::class, 'index'])->name('products.index');
        Route::get('/create', [ProductsController::class, 'create'])->name('products.create');
        Route::post('/store', [ProductsController::class, 'store'])->name('products.store');
        Route::get('/show/{id}', [ProductsController::class, 'show'])->name('products.show');
        Route::get('/edit/{id}', [ProductsController::class, 'edit'])->name('products.edit');
        Route::post('/update/{id}', [ProductsController::class, 'update'])->name('products.update');
        Route::post('/product/delete', [ProductsController::class, 'delete'])->name('products.delete');
    });
    
    //Slider Routes
    Route::prefix('sliders')->group(function(){
        Route::get('/', [SlidersController::class, 'index'])->name('sliders.index');
        Route::get('/create', [SlidersController::class, 'create'])->name('sliders.create');
        Route::post('/store', [SlidersController::class, 'store'])->name('sliders.store');
        Route::get('/edit/{id}', [SlidersController::class, 'edit'])->name('sliders.edit');
        Route::post('/update/{id}', [SlidersController::class, 'update'])->name('sliders.update');
        Route::post('/slider/delete', [SlidersController::class, 'delete'])->name('sliders.delete');
    });
    
    //Order Routes
    Route::prefix('orders')->group(function(){
        Route::get('/pending/view', [OrdersController::class, 'pendingList'])->name('orders.pending.view');
        Route::get('/approved/view', [OrdersController::class, 'approvedList'])->name('orders.approved.view');
        Route::get('/details/{id}', [OrdersController::class, 'details'])->name('orders.details');
        Route::post('/approved', [OrdersController::class, 'approved'])->name('orders.approved');
    });
    
    
    
});

