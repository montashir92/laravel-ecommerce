<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Size;

class SizesController extends Controller
{
    
    public function index()
    {
        $data['allData'] = Size::all();
        return view('backend.pages.sizes.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.pages.sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:sizes,name',
        ]);
        
        $sizes = new Size();
        $sizes->name = $request->name;
        $sizes->save();
        return redirect()->route('sizes.create')->with('toast_success', 'A New Size Added Successfully');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['editData'] = Size::find($id);
        return view('backend.pages.sizes.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $sizes = Size::find($id);
        $sizes->name = $request->name;
        $sizes->save();
        return redirect()->route('sizes.index')->with('toast_success', 'Size Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $size = Size::find($request->id);
        if(!is_null($size))
        {
            $size->delete();
        }
        return redirect()->back()->with('toast_success', 'Size Data Deleted Successfully');
    }
}
