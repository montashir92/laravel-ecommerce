<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductColor;
use App\Models\ProductSize;

class ProductController extends Controller
{
    
    public function index()
    {
        $data['products'] = Product::orderBy('id', 'desc')->paginate(12);
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['brands'] = Product::select('brand_id')->groupBy('brand_id')->get();
        return view('frontend.pages.products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryProduct($category_id)
    {
        $data['products'] = Product::orderBy('id', 'desc')->where('category_id', $category_id)->get();
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['brands'] = Product::select('brand_id')->groupBy('brand_id')->get();
        return view('frontend.pages.products.category_product', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function brandProduct($brand_id)
    {
        $data['products'] = Product::orderBy('id', 'desc')->where('brand_id', $brand_id)->get();
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['brands'] = Product::select('brand_id')->groupBy('brand_id')->get();
        return view('frontend.pages.products.brand_product', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data['product'] = Product::where('slug', $slug)->first();
        $data['sub_images'] = ProductImage::where('product_id', $data['product']->id)->get();
        $data['product_color'] = ProductColor::where('product_id', $data['product']->id)->get();
        $data['product_size'] = ProductSize::where('product_id', $data['product']->id)->get();
        return view('frontend.pages.products.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $slug = $request->search;
        $product = Product::where('slug', $slug)->first();
        if($product)
        {
            $data['product'] = Product::where('slug', $slug)->first();
            $data['sub_images'] = ProductImage::where('product_id', $data['product']->id)->get();
            $data['product_color'] = ProductColor::where('product_id', $data['product']->id)->get();
            $data['product_size'] = ProductSize::where('product_id', $data['product']->id)->get();
            return view('frontend.pages.products.product_search', $data);
        }else{
            return redirect()->back()->with('warning', 'Product Does Not Match');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProduct(Request $request)
    {
        $slug = $request->slug;
        $product = Product::where('slug', 'LIKE', '%'.$slug.'%')->get();
        
        $html = '';
        $html .= '<div><ul>';
        if($product)
        {
            foreach ($product as $v) {
               $html .= '<li>'.$v->slug.'</li>'; 
            }
        }
        $html .= '</ul></div>';
        return response()->json($html);
    }

}
